package com.example.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.naming.Name;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer pId;


    private  String pName;
    private String pAddrs;

    @OneToMany(targetEntity = PhoneNumber.class,cascade = CascadeType.ALL)
    @JoinColumn(name = "PERSON_ID",referencedColumnName = "PID")// PK Column
    private Set<PhoneNumber> contactDetails;//for one to many


}
