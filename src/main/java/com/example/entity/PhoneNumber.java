package com.example.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class PhoneNumber implements Serializable {

    @Id
    @SequenceGenerator(name = "gen1", sequenceName = "regNo_seq",initialValue = 1000,allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "gen1")
    private Long phoneNumber;
    private String provider;
    private String type;

    @ManyToOne(targetEntity = Person.class,cascade = CascadeType.ALL)
    @JoinColumn(name = "PERSON_ID",referencedColumnName = "PID")
    private Person person;


}
