package com.example.Service;

import com.example.Repository.IPersonRepo;
import com.example.Repository.IPhoneNumberRepo;
import com.example.entity.Person;
import com.example.entity.PhoneNumber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class ServiceIMpl {
    @Autowired
    private IPersonRepo personRepo;

    @Autowired
    private IPhoneNumberRepo phoneRepo;

    public String saveDataUsingParent(Person person){
        return "data Save Successfully"+personRepo.save(person).getPId();
    }

    public ResponseEntity<?> deletePerson(Integer id) {
        System.out.println("ServiceIMpl.deletePerson");
        Optional<Person> opt =personRepo.findById(id);
        if(opt.isPresent()){
            System.out.println("ServiceIMpl.deletePerson");
            Set<PhoneNumber> child = opt.get().getContactDetails();
            child.forEach(ph->{
                System.out.println("ServiceIMpl.deletePerson");
                ph.setPerson(null);
            });
            phoneRepo.deleteAll();
            return new ResponseEntity<>(child.size()+" phoneNumber of "+id+" person are deleted ",HttpStatus.OK);
        }
        return new ResponseEntity<>("Recode NOt Found",HttpStatus.NOT_FOUND);
//        return personRepo.findById(id).map(post -> {
//            personRepo.delete(post);
//            return ResponseEntity.ok().build();
//        }).orElseThrow(() -> new NullPointerException("PostId " + id + " not found"));
    }

    public ResponseEntity<?> saveGroup(Iterable<Person> people) {
        return  new ResponseEntity<>(personRepo.saveAll(people), HttpStatus.CREATED);
    }
}
