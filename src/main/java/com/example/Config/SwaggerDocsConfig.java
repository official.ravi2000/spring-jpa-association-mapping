package com.example.Config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerDocsConfig {

    @Bean
    public Docket createDocket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.Controller"))//base package of restController
                .paths(PathSelectors.any())//to specify requestPath
                .build()
                .useDefaultResponseMessages(true)
                .apiInfo(getAipInfo());
    }

    private ApiInfo getAipInfo(){
        Contact contact = new Contact("Ravindra","ravi......","official.ravi2000@gmail.com");
        return  new ApiInfo("Person API", "Save Person Details","3.4 Release","http://www.hcl.com/license",contact,"GNU Public ","", Collections.emptyList());

    }
}
