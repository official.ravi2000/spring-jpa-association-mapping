package com.example.Repository;

import com.example.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPersonRepo extends JpaRepository<Person,Integer> {
}
