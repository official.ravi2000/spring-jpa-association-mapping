package com.example.Repository;

import com.example.entity.PhoneNumber;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPhoneNumberRepo extends JpaRepository<PhoneNumber,Integer> {
}
