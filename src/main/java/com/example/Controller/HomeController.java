package com.example.Controller;

import com.example.Service.ServiceIMpl;
import com.example.entity.Person;
import com.example.entity.PhoneNumber;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @Autowired
    private ServiceIMpl service;

    private Logger logger = LoggerFactory.getLogger(HomeController.class);
    @PostMapping("/savePerson")
    public String savePerson(@RequestBody Person person){
    	logger.info("save method execution statred  for "+person.getPId());
        return  service.saveDataUsingParent(person);
    }

    @DeleteMapping("/delete/{Id}")
    public ResponseEntity<?> deletePerson(@PathVariable Integer Id){
    	logger.info("delete method Execution started"+Id);
        return service.deletePerson(Id);
    }

    @PostMapping("/saveGroup")
    public ResponseEntity<?> saveGroup(@RequestBody Iterable<Person> people){
        return  service.saveGroup(people);
    }
}
